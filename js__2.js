/*
  $$addUsers fills array users their names. 
  parameters:
  (digit) $numberOfCyccle - number of users.
  $$addUsers will return array with users to $numberOfUsers. 
*/
function addUsers(numberOfCycle) {
  var numberOfUsers = [];
  
  for(var i = 0; numberOfCycle > i; i++) {
    var user = prompt('Введите имя ' + (i + 1) + ' пользователя', '');
    numberOfUsers[i] = user;
  }
  return numberOfUsers;
}

/*
  $$login check array $uersArray haved admin or not. 
  parameters:
  (digit) $adminName - var with admin name.
  (digit) $usersArray - number of users.
  $$login will return true or error. 
*/
function login(adminName, usersArray) {
  for(var i = 0; i < usersArray.length; i++) {
    if (usersArray[i] == adminName) {
      alert('Вы успешно вошли ' + usersArray[i]);
      return true;
    }
  }
  
  alert('Ошибка входа');
}

/*
  script start point 
*/
function main() {
  var numberOfCycle = 5; //number of users
  
  /*when  $$addUsers executed, $numberOfUsers returned value into $usersArray*/
  usersArray = addUsers(numberOfCycle);
  adminName = prompt('Введите имя администратора','');//input admin name
  login(adminName, usersArray); 
}

main()

