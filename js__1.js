function degreeUserNumber() {
  /*
    $$powNumber powered digits and return it.
    parameters:
    (digit) $num - value which be elevated,
    (digit) $pow - value which will be degree
    $$powNumber will return powered digit to $num
  */
  function powNumber(num, pow) {
    if (pow == 0)
      num = 1;
    else
      while (pow > 1 || pow < -1) {
        num *= num;
        pow > 1 ? pow-- : ++pow;
        
        if (pow == -1)
          num = 1/num;
      }
    return num;
  }

  var userVar = prompt('Введите число', ''); //user need input digit, value entered in $userVar
  var userPow = prompt('Введите степень', ''); //user need input degree, value entered in $userPow
  //first transform text to number and then check what number is real number
  if((isFinite(parseFloat(userVar)) ) && 
     (isFinite(parseFloat(userPow)) )) {
    //when  $$powNumber executed, $num returned value into $result
    result = powNumber(userVar, userPow); 
    console.log(result); //show $result
  } else {
    console.log("input error");
  }
}

degreeUserNumber();